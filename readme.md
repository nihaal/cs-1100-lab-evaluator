### CS1100 April-June Section1 Lab Evaluator  

This tool can be used to evaluate private test-cases stored in encrypted format on public repository like `repl.it`. Read the following instructions.

#### Instructions   for Problem Setters

1. Run `python3 bin/generatekey.py` on your terminal to generate a pass key stored in `project/pass.key` file. This key will be used to encrypt the test-cases. **Do not share this key with anyone other than TAs**.
2. For each question, make a copy of this project. Write a program in `project/main.c` for the question.
3. Write the inputs for the test cases in `project/inputs.txt`. different test-cases are separated by `&` as shown below:

    ```
    1
    &
    2
    &
    3
    ```
4. Write the outputs for the test cases in `project/outputs.txt`. Similar to the inputs.txt file, different test-cases are separated by `&` as shown below:

    ```
    1
    &
    2
    &
    3
    ```
5. Run `python3 bin/store_cases.py` for each of question. **Verify the output of your programs printed on the console.** 
6. Upload the newly created `project/eval.py` in the repl project for every question. 
    
    **Note:** `project/eval.py` is generated using `templates/template.py` file. The `project/eval.py` file will be visible to the student and it will contain the test cases and the `templates/test.py` file in encrypted format.
7. Share `project/pass.key ` file with TAs.

Please see the **modification** section if you want to use it for other languages or your input may contain `&` symbol. 

<br></br>

#### For Evaluators

1. In terminal type `python3 eval.py`,  use option `-v` for printing the test-cases.  
2. Paste the passkey shared with you. Paste using `ctrl-shift-v` or use right click and paste. Remember that the pasted key won't echo on the console for security purpose.   
3. Press Enter.



#### Modifications

**1. My Input or Output Contains `&` symbol:**
There are two resolutions for this:
1. Change the `ip_sep` variable defined in `bin/utility.py` file to some other symbol not used in the inputs.
2. Try changing `inputs = [ip.strip() for ip in inputs.split(ip_sep)]` line in file `bin/store_cases.py` to accept inputs as per your requirement.

**2. I want to use other languages**
1. Change the following lines in `bin/utility.py` accordingly.
    
    ```python
    # Compiling the code in main.c, change if needed for other language.
    cmd = f'gcc {PROJECT_PATH}/main.c -o {PROJECT_PATH}/main'
    os.system(cmd)
    ```
2. Change the command for executing the code by changing `executable` variable in `bin/utility.py` and `templates/test.py` accordingly (Not needed for cpp).

    For example if I want to use **python**, I can comment the code used for compilation and change the `executable` variable as following:
    
    ```python
    executable=f'python3 {PROJECT_PATH}/main.py'
    ```

    **Note**:`PROJECT_PATH` variable provides an absolute path of the files.
    
**3. I don't want to use exact matching of the outputs**
   - Change the `match_output` function defined in `templates/test.py` for the particular question. You are free to use any mechanism you may want to use for matching.

<br></br>

\--
Author: Dhairya Bhardwaj.
M.Tech., Department of Computer Science and Engineering, 
IIT Madras.
smail: cs19m022@smail.iitm.ac.in



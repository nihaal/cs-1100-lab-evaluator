import json
from os.path import dirname, join, abspath
import sys

HOME = abspath(join(dirname(__file__),'..'))
sys.path.append(HOME)

from bin.utility import *


with open(f'{PROJECT_PATH}/inputs.txt','r') as f:
    inputs = f.read()

with open(f'{PROJECT_PATH}/outputs.txt','r') as f:
    outputs = f.read()

inputs = [ip.strip() for ip in inputs.split(ip_sep)]
outputs = [ip.strip() for ip in outputs.split(ip_sep)]

print(f'There are {len(inputs)} test cases as following, please verify')

i = 1
for ip,op in zip(inputs,outputs):
    print(f"Test-case {i}:")
    print(f"Input: \n{ip}")
    print(f"Output: \n{op}")
    print()
    i+=1  


testcases = {
    'inputs':inputs,
    'outputs':outputs
}

json_testcase = json.dumps(testcases)


try:
    with open(f'{PROJECT_PATH}/pass.key','rb') as f:
        passkey = f.read()
except:
    print("passkey file not found, please run generatekey.py")
    exit(-1)

encrypted_testcase = encrypt(json_testcase,passkey)

try:
    with open(f'{TEMPLATE_PATH}/test.py','r') as f:
        testpy = f.read()
except:
    print("test.py file not found, Aborting")
    exit(-1)

encrypted_testpy = encrypt(testpy,passkey)

try:
    with open(f'{TEMPLATE_PATH}/template.py','r') as f:
        template = f.read()
except:
    print("template.py file not found, Aborting")
    exit(-1)

try:
    with open(f'{PROJECT_PATH}/eval.py','w') as f:
        f.write(template.format(encrypted_testcase,encrypted_testpy))
except:
    print("Couldn't create encrypted file")
    exit(-1)
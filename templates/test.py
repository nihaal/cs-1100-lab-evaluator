import json
import sys
import platform 
import pty


from os.path import dirname, join, abspath


HOME = abspath(join(dirname(__file__),'.'))
sys.path.append(HOME)
PROJECT_PATH = HOME

# The command to execute the student's project, it is ./main by default in repl.it platform
current_os = platform.system().lower()
executable = f"{PROJECT_PATH}/main.exe" if current_os=="windows" else f"{PROJECT_PATH}/main"

def match_outputs(expected:str, printed:str):
    '''
    Defines the matching function for the outputs of student's code to the expected output.
    
    expected: the output which was expected.
    printed: output of student's program.
    
    You can implement function to match floating point numbers with a precision.
    You can also use complex matching mechanism using regex. 

    return expected.strip()==printed.strip() returns true if the expected and printed outputs match exactly.
    '''
    return expected.strip()==printed.strip()



#!!!!!!! The following code doesn't require any change. (in most of the cases) !!!!!!!


def call_process(executable,ip,tc):
    from subprocess import run, PIPE
    try:
        process = run(executable.split(), stdout=PIPE, stderr=PIPE,
                input=ip, encoding='ascii')
    except:
        print(f"{executable} file not found, try compiling the code")
        exit(-1)
    if(process.returncode!=0):
        print(f"Testcase {tc}: Non zero return code({process.returncode}):{process.stderr} (Possibly Segmentation Fault)")
        return ''
    return process.stdout


try:
    # decrypt, tc_enc and passkey are defined in template.
    testcases = decrypt(tc_enc,passkey)
except:
    print("Wrong pass key or pasted incorrectly try ctrl-shift-v or right click and paste")
    exit(-1)

try:
    testcases = json.loads(testcases)
except:
    print("The key used for decrypt is not the same used for encrypting")
    exit(-1)

inputs = testcases['inputs']
outputs = testcases['outputs']

i = 1
score = 0
total = len(inputs)

for ip,op in zip(inputs,outputs):
    op2 = call_process(executable,ip,i)
    if len(sys.argv)>1 and (sys.argv[1]=="-v" or sys.argv[1]=="-V"):
        print(f"Testcase {i}:")
        print(f"Input: \n{ip}")
        print(f"Output: \n{op2}")
        print(f'Expected Output: \n{op}')

    if match_outputs(op,op2):
        print(f"Testcase {i}: Passed")
        score+=1
    
    else:
        print(f"Testcase {i}: Failed")

    print()
    i+=1 

print(f"The total score is {score}/{total}")
